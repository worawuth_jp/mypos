<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User as User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->username = "admin";
        $user->password = Hash::make('admin2542');
        $user->save();
    }
}
