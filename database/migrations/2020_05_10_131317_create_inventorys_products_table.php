<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInventorysProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventories_products', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('inventories_id');
            $table->unsignedBigInteger('products_id');
            $table->foreign('inventories_id')->references('id')->on('inventories');
            $table->foreign('products_id')->references('id')->on('products');
            $table->boolean('isDelete')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventorys_products');
    }
}
