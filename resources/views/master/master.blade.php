<?php session_start(); ?>
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('partials.head')
</head>
<body style="background-color: #E8E8E8" class="overflow-auto hold-transition sidebar-mini layout-fixed">
@include('partials.nav')
@include('partials.sidebar')

<div class="content-wrapper">
  @yield('body')
</div>

@include('partials.footer')
@yield('footer')
</body>
</html>
