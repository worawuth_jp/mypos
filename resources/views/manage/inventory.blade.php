@extends('master.master')

@section('body')
    <main role="main" class="container col-12 pt-5" style="height: 90vh">
        <section class="content">
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark">คลังสินค้า</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                                <li class="breadcrumb-item active">คลังสินค้า</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->

                    <div class="row">
                        <div class="col-lg-3 col-6">
                            <!-- small box -->
                            <a href="#">
                                <div class="small-box bg-gray cursor-hand">
                                    <div class="inner">
                                        <h3>150</h3>

                                        <p>สินค้าทั้งหมด</p>
                                    </div>
                                    <div class="icon">
                                        <i class="ion ion-cube"></i>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-2">
                            <!-- small box -->
                            <a data-toggle="modal" data-target="#addProductForm">
                                <div class="small-box bg-gray-light cursor-hand">
                                    <div class="inner text-center align-items-center">
                                        <h3><i class="ion ion-ios-plus "></i></h3>
                                        <p>เพิ่มสินค้า</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>{{--/row--}}

                    <div class="row">
                        <!-- Left col -->
                        <section class="col-12 connectedSortable">
                            <!-- Custom tabs (Charts with tabs)-->
                            <div class="card">
                                <div class="card-header bg-info">
                                    <i class="ion ion-cube mt-2" style="font-size: 2em">&nbsp;</i>
                                    <label class="header col-form-label" style="font-size: 1.5em">คลังสินค้า</label>
                                </div><!-- /.card-header -->
                                <div class="card-body">
                                    <table id="inventory" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>ชื่อสินค้า</th>
                                            <th>ประเภทการขาย</th>
                                            <th>ราคาขาย</th>
                                            <th>จำนวน</th>
                                            <th>บาร์โค๊ด</th>
                                            <th>สต็อก</th>
                                            <th>จัดการ</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>ครีมโอเลย์</td>
                                            <td>ขายปลีก</td>
                                            <td>95</td>
                                            <td> 4</td>
                                            <td>1710005555</td>
                                            <td>1-07/05/2563</td>
                                            <td>
                                                แก้ไข ลบ ดู
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>ครีมbest</td>
                                            <td>ขายส่ง</td>
                                            <td>400</td>
                                            <td> 10</td>
                                            <td>1710005555</td>
                                            <td>1-07/05/2563</td>
                                            <td>
                                                แก้ไข ลบ ดู
                                            </td>
                                        </tr>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>ชื่อสินค้า</th>
                                            <th>ประเภทการขาย</th>
                                            <th>ราคาขาย</th>
                                            <th>จำนวน</th>
                                            <th>บาร์โค๊ด</th>
                                            <th>สต็อก</th>
                                            <th>จัดการ</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div><!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </section>
                        <!-- /.Left col -->
                    </div>
                    <!-- /.row (main row) -->
                </div><!-- /.container-fluid -->
            </div>
        </section>
        <!--Modal Form-->
        <div class="modal fade" id="addProductForm">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">เพิ่มสินค้าใหม่</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form class="form-group" method="post" id="addProductForm" enctype="multipart/form-data">
                        @csrf
                        <div class="modal-body ">
                            <span id="showResult">

                            </span>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <label class="col-form-label">ชื่อสินค้า : </label>
                                </div>
                                <div class="col-8">
                                    <input class="input-outline-green form-control" maxlength="100" type="text"
                                           name="productName" id="productName" placeholder="ใส่ชื่อสินค้า">
                                </div>
                            </div>
                            <div class="row pt-2">
                                <div class="col-3 text-right">
                                    <label class="col-form-label">ราคาขาย : </label>
                                </div>
                                <div class="col-3">
                                    <input class="input-outline-green form-control" maxlength="100" type="text"
                                           name="pricesell" id="pricesell" placeholder="ราคาขาย">
                                </div>
                                <div class="col-2 text-right">
                                    <label class="col-form-label">หน่วย : </label>
                                </div>
                                <div class="col-3">
                                    <input class="input-outline-green form-control" maxlength="100" type="text"
                                           name="unit" id="unit" placeholder="หน่วยสินค้า">
                                </div>
                            </div>

                            <div class="row pt-3">
                                <div class="col-3 text-right">
                                    <label class="col-form-label">ประเภทการขาย : </label>
                                </div>
                                <div class="col-3">
                                    <select name="selltype" id="stype" class="form-control">
                                        <option>---เลือกรายการ---</option>
                                        <option>ขายปลีก</option>
                                        <option>ขายส่ง</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row pt-3">
                                <div class="col-3 text-right">
                                    <label class="col-form-label">barcode : </label>
                                </div>
                                <div class="col-8">
                                    <input class="input-outline-green form-control" maxlength="100" type="text"
                                           name="barcode" id="barcode" placeholder="รหัสบาร์โค๊ด">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer pb-0 pt-1">
                            <input type="hidden" name="action" id="action" value="add"/>
                            <input type="hidden" name="hidden_id" id="hidden_id" value="{{ csrf_token() }}">
                            <input type="submit" name="submit" value="เพิ่มสินค้า" id="postProductBtn"
                                   class="btn btn-outline-primary">
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!--/Modal Form-->

    </main>
@endsection

@section('footer')
    <script>
        $(function () {
            $("#inventory").DataTable({
                "responsive": true,
                "autoWidth": false,
            });
        });
    </script>

    <script>
        $(document).ready(function () {
            $('#postProductBtn').click(function () {
                $.post('{{url('postdata')}}',{
                    product: $('#productName').val()
                },
                    function (data) {
                    alert(data);
                    }
                )
            });
        });
    </script>
@endsection
