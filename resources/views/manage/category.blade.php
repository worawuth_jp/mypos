@extends('master.master')

@section('body')
    <main role="main" class="container col-12 pt-5" style="height: 90vh">
        <section class="content">
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark">หมวดหมู่สินค้า</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                                <li class="breadcrumb-item active">หมวดหมู่สินค้า</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                    <div class="row">
                        <div class="col-lg-3 col-6">
                            <!-- small box -->
                            <a href="#">
                                <div class="small-box bg-gray cursor-hand">
                                    <div class="inner">
                                        <h3>12</h3>

                                        <p>หมวดหมู่สินค้า</p>
                                    </div>
                                    <div class="icon">
                                        <i class="ion ion-clipboard"></i>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-2">
                            <!-- small box -->
                            <a data-toggle="modal" data-target="#addCategoryForm">
                                <div class="small-box bg-gray-light cursor-hand">
                                    <div class="inner text-center align-items-center">
                                        <h3><i class="ion ion-ios-plus "></i></h3>
                                        <p>เพิ่มหมวดหมู่</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>{{--/row--}}

                    <div class="row">
                        <!-- Left col -->
                        <section class="col-12 connectedSortable">
                            <!-- Custom tabs (Charts with tabs)-->
                            <div class="card">
                                <div class="card-header bg-info">
                                    <i class="ion ion-cube mt-2" style="font-size: 2em">&nbsp;</i>
                                    <label class="header col-form-label" style="font-size: 1.5em">หมวดหมู่สินค้า</label>
                                </div><!-- /.card-header -->
                                <div class="card-body">
                                    <table id="categoryTable" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>ชื่อหมวดหมู่</th>
                                            <th>จำนวนสินค้าในหมวดหมู่</th>
                                            <th>จัดการ</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>ครีม</td>
                                            <td>25</td>
                                            <td>แก้ไข ลบ ดู</td>
                                        </tr>
                                        <tr>
                                            <td>เครื่องสำอาง</td>
                                            <td>20</td>
                                            <td>แก้ไข ลบ ดู</td>
                                        </tr>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>ชื่อหมวดหมู่</th>
                                            <th>จำนวนสินค้าในหมวดหมู่</th>
                                            <th>จัดการ</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div><!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </section>
                        <!-- /.Left col -->
                    </div>
                    <!-- /.row (main row) -->
                </div><!-- /.container-fluid -->
            </div>
        </section>
        <!--Modal Form-->
        <div class="modal fade" id="addCategoryForm">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">เพิ่มสินค้าใหม่</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body ">
                        <h5 class="pb-1 pt-1">ชื่อหมวดหมู่ : <input class="p-2 col-10 font-weight-normal" type="text"
                                                                    style="font-size: 18px"
                                                                    placeholder="ใส่ชื่อหมวดหมู่">
                        </h5>
                    </div>
                    <div class="modal-footer d-flex align-self-center">
                        <button type="button" class="btn btn-outline-primary">เพิ่ม</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!--/Modal Form-->

    </main>
@endsection

@section('footer')
    <script>
        $(function () {
            $("#categoryTable").DataTable({
                "responsive": true,
                "autoWidth": false,
            });
        });

    </script>
@endsection

