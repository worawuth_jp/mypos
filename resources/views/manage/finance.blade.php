@extends('master.master')

@section('body')
    <main role="main" class="container col-12 pt-5" style="height: 90vh">
        <section class="content">
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark">ระบบการเงิน</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                                <li class="breadcrumb-item active">finance</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                    <div class="row">
                        <div class="col-lg-3 col-6">
                            <!-- small box -->
                            <a href="#">
                                <div class="small-box bg-gray cursor-hand">
                                    <div class="inner">
                                        <h3>10</h3>

                                        <p>รายการยอดจ่ายรายเดือน</p>
                                    </div>
                                    <div class="icon">
                                        <i class="ion ion-calendar"></i>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-2">
                            <!-- small box -->
                            <a data-toggle="modal" data-target="#addList">
                                <div class="small-box bg-gray-light cursor-hand">
                                    <div class="inner text-center align-items-center">
                                        <h3><i class="ion ion-ios-plus "></i></h3>
                                        <p>เพิ่มรายการใหม่</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>{{--/row--}}

                    <!-- /.row (main row) -->
                </div><!-- /.container-fluid -->
            </div>
            <div class="card">
                <div class="card-header bg-green">
                    <label class="pl-2 col-form-label">ข้อมูลเงินเดือนและรายจ่ายรายเดือน</label>
                </div>
                <div class="card-body">
                    <table id="List" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>ชื่อสินค้า</th>
                            <th>ประเภทการขาย</th>
                            <th>ราคาขาย</th>
                            <th>จำนวน</th>
                            <th>บาร์โค๊ด</th>
                            <th>สต็อก</th>
                            <th>จัดการ</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>ครีมโอเลย์</td>
                            <td>ขายปลีก</td>
                            <td>95</td>
                            <td> 4</td>
                            <td>1710005555</td>
                            <td>1-07/05/2563</td>
                            <td>
                                แก้ไข ลบ ดู
                            </td>
                        </tr>
                        <tr>
                            <td>ครีมbest</td>
                            <td>ขายส่ง</td>
                            <td>400</td>
                            <td> 10</td>
                            <td>1710005555</td>
                            <td>1-07/05/2563</td>
                            <td>
                                แก้ไข ลบ ดู
                            </td>
                        </tr>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>ชื่อสินค้า</th>
                            <th>ประเภทการขาย</th>
                            <th>ราคาขาย</th>
                            <th>จำนวน</th>
                            <th>บาร์โค๊ด</th>
                            <th>สต็อก</th>
                            <th>จัดการ</th>
                        </tr>
                        </tfoot>
                    </table>
                </div><!-- /.card-body -->
            </div>
        </section>
    </main>
    <!--Modal Form-->
    <div class="modal fade" id="addList">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">เพิ่มสินค้าใหม่</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body ">
                    <h5 class="pb-1 pt-1">ชื่อหมวดหมู่ : <input class="p-2 col-10 font-weight-normal" type="text"
                                                                style="font-size: 18px"
                                                                placeholder="ใส่ชื่อหมวดหมู่">
                    </h5>
                </div>
                <div class="modal-footer d-flex align-self-center">
                    <button type="button" class="btn btn-outline-primary">เพิ่ม</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!--/Modal Form-->
@endsection

@section('footer')
    <script>
        $(function () {
            $("#List").DataTable({
                "responsive": true,
                "autoWidth": false,
            });
        });
    </script>
@endsection

