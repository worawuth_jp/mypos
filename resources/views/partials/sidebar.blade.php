<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{URL('/')}}" class="brand-link">
        <img src="https://static.vecteezy.com/system/resources/thumbnails/000/376/355/small/Universal__28436_29.jpg" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light">POS MANAGEMENT</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item has-treeview menu-open">
                    <a href="{{url('inventory')}}" class="nav-link bg-warning">
                        <i class="fa fa-truck nav-icon"></i>
                        <p>
                            ระบบจัดการคลังสินค้า
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{url('inventory')}}" class="nav-link ">
                                <i class="fa fa-circle nav-icon"></i>
                                <p>คลังสินค้า</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('category')}}" class="nav-link">
                                <i class="fa fa-circle nav-icon"></i>
                                <p>หมวดหมูสินค้า</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('stock')}}" class="nav-link">
                                <i class="fa fa-circle nav-icon"></i>
                                <p>สต็อกสินค้า</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('barcode')}}" class="nav-link">
                                <i class="fa fa-circle nav-icon"></i>
                                <p>barcode</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('supply')}}" class="nav-link">
                                <i class="fa fa-circle nav-icon"></i>
                                <p>Supply</p>
                            </a>
                        </li>

                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="{{url('inventory')}}" class="nav-link bg-warning">
                        <i class="fa fa-usd nav-icon"></i>
                        <p>
                            ระบบบริหารเงิน
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{url('finance')}}" class="nav-link ">
                                <i class="fa fa-circle nav-icon"></i>
                                <p>การเงิน</p>
                            </a>
                        </li>

                    </ul>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="{{url('shopManagement')}}" class="nav-link bg-warning">
                        <i class="fa fa-shopping-bag nav-icon"></i>
                        <p>
                            ระบบบริหารร้านค้า
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{url('shopManagement')}}" class="nav-link ">
                                <i class="fa fa-circle nav-icon"></i>
                                <p>บริการร้านค้า</p>
                            </a>
                        </li>

                    </ul>
                </li>
                <li class="nav-item">
                    <a href="{{url('user')}}" class="nav-link bg-warning">
                        <i class="nav-icon fa fa-user-circle"></i>
                        <p>
                            user management
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
