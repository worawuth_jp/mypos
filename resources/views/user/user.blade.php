@extends('master.master')

@section('body')
    <main role="main" class="container col-12 pt-5" style="height: 90vh">
        <section class="content">
            <div class="content-header pb-2">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark">จัดการผู้ใช้ระบบ</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                                <li class="breadcrumb-item active">user</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->

                    <!-- /.row (main row) -->
                </div><!-- /.container-fluid -->
            </div>
            <div class="card mt-0">
                <div class="card-header bg-info">
                    <label class="pl-2 col-form-label" style="font-size: 1.5em">ลงทะเบียนผู้เข้าใช้ระบบใหม่</label>
                </div>
                <div class="card-body">
                    <form class="form-group" method="post" action="{{url('register')}}">
                        <div class="row">
                            <div class="col-3 text-right">
                                <label class="col-form-label font-weight-bold">
                                    username :
                                </label>
                            </div>
                            <div class="col-3">
                                <input type="text" name="username" class="input-group form-control">
                            </div>
                        </div>
                        <div class="row pt-2">
                            <div class="col-3 text-right">
                                <label class="col-form-label font-weight-bold">
                                    password :
                                </label>
                            </div>
                            <div class="col-3">
                                <input type="password" name="password" class="input-group form-control">
                            </div>
                        </div>
                        <div class="row pt-2">
                            <div class="col-3 text-right">
                                <label class="col-form-label font-weight-bold">
                                    confirm password :
                                </label>
                            </div>
                            <div class="col-3">
                                <input type="password" name="confirmPass" class="input-group form-control">
                            </div>
                        </div>
                        <div class="row pt-5 container">
                            <div class="col-3"></div>
                            <div class="col-2 text-center ml-0">
                                <button class="form-control btn btn-outline-dark">ย้อนกลับ</button>
                            </div>
                            <div class="col-2 text-right mr-0">
                                <input type="submit" value="register" class="form-control btn btn-outline-dark">
                            </div>
                            <div class="col-4">

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </main>

@endsection

@section('footer')

@endsection

