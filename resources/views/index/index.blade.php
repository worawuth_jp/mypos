@extends('master.master')

@section('body')
    <main role="main" class="container col-12 pt-5" style="height: 90vh">
        <section class="content">
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark">Dashboard</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                                <li class="breadcrumb-item active">Dashboard</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
        </section>

        <div class="row">
            <div class="col-lg-3">
                <div class="card">
                    <div class="card-header bg-gray" style="font-size: 1.2em">
                        MANAGEMENT SYSTEM USER
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <label class="font-weight-bold">Username:&nbsp;</label>
                            <label class="font-weight-light">{{Auth::user()->username}}</label>
                        </div>
                        <div class="row">
                            <label class="font-weight-bold">status:&nbsp;</label>
                            <label class="font-weight-light">admin</label>
                        </div>
                        <div class="row">
                            <label class="font-weight-bold">password:&nbsp;</label>
                            <a href="#" style="text-decoration: underline" data-toggle="modal" id="changePass"
                               data-target="#passwordForm">เปลี่ยนรหัสผ่าน</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card">
                    <div class="card-header bg-info " style="font-size: 1.2em">
                        รายงาน
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-2
                                 text-right pr-2">
                                <label for="showSellBalance" class="col-form-label" style="font-size: 1.1em"><i
                                        class="fa fa-shopping-basket cl-green" aria-hidden="true">&nbsp;</i>ยอดขายวันนี้
                                    :
                                </label>
                            </div>
                            <div class="text-left pl-2 pr-2 border-green">
                                <label id="showSellBalance" class="cl-green" style="font-size: 1.5em">
                                    4,500.00
                                </label>
                            </div>

                            <div class="col-lg-2 text-right pr-2">
                                <label for="showSellCost" class="col-form-label" style="font-size: 1.1em">
                                    <i class="fa fa-check-square" aria-hidden="true">&nbsp;</i>
                                    ค่าใช้จ่าย :
                                </label>
                            </div>
                            <div class="text-left pl-2 pr-2">
                                <label id="showExpense" class="cl-cost" style="font-size: 1.5em">
                                    {{number_format($sumcost,2,'.',',')}}
                                </label>
                            </div>

                            <div class="pl-3 form-row">
                                <button data-toggle="modal" data-target="#addSelfCost" class="button-darkgray">
                                    <label class="cursor-hand font-weight-light cl-white" style="font-size: 1.1em">เพิ่มค่าใช้จ่าย</label>
                                </button>
                            </div>
                        </div>
                        <div class="row pt-1">
                            <div class="col-2 text-right pr-2">
                                <label for="showCost" class="col-form-label" style="font-size: 1.1em"><i
                                        class="fa fa-cubes cl-cost" aria-hidden="true">&nbsp;</i>ต้นทุน
                                    :</label>
                            </div>
                            <div class="text-left pl-2 pr-2 border-red">
                                <label id="showCost" class="cl-cost" style="font-size: 1.5em">
                                    2,500.00
                                </label>
                            </div>

                            <div class="col-2 text-right pr-2">
                                <label for="showSellBalance" class="col-form-label " style="font-size: 1.1em">
                                    <i class="fa fa-credit-card cl-profit" aria-hidden="true">&nbsp;</i>
                                    กำไร :
                                </label>
                            </div>
                            <div class="text-left pl-2 pr-2 border-orange">
                                <label id="showSellBalance" class="cl-profit" style="font-size: 1.5em">
                                    1,500.00
                                </label>
                            </div>

                            <div class="col-2 text-right pr-2">
                                <label for="showSellBalance" class="col-form-label " style="font-size: 1.1em">
                                    <i class="fa fa-television cl-theme" aria-hidden="true">&nbsp;</i>
                                    คงเหลือ :
                                </label>
                            </div>
                            <div class="text-left pl-2 pr-2">
                                <label id="showSellBalance" class="cl-balance" style="font-size: 1.5em">
                                    1,000.00
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card contrainer">
            <div class="card-header bg-warning " style="font-size: 1.2em">
                รายการที่ขายในวันนี้
            </div>
            <div class="card-body">

                <table id="sellBalance" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>ชื่อสินค้า</th>
                        <th>ประเภทการขาย</th>
                        <th>ราคาขาย</th>
                        <th>จำนวน</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>ครีมโอเลย์</td>
                        <td>ขายปลีก</td>
                        <td>95</td>
                        <td> 4</td>
                    </tr>
                    <tr>
                        <td>ครีมbest</td>
                        <td>ขายส่ง</td>
                        <td>400</td>
                        <td> 10</td>
                    </tr>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>ชื่อสินค้า</th>
                        <th>ประเภทการขาย</th>
                        <th>ราคาขาย</th>
                        <th>จำนวน</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>

        <div class="card contrainer">
            <div class="card-header bg-info">
                <label class="header col-form-label" style="font-size: 1.2em">จัดลำดับสินค้าขายดี</label>
            </div>
            <div class="card-body">
                <table id="monthSEll" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>ลำดับ</th>
                        <th>ชื่อสินค้า</th>
                        <th>ประเภทการขาย</th>
                        <th>จำนวน</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>ครีมโอเลย์</td>
                        <td>ขายปลีก</td>
                        <td> 4</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>ครีมbest</td>
                        <td>ขายส่ง</td>
                        <td> 10</td>
                    </tr>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>ลำดับ</th>
                        <th>ชื่อสินค้า</th>
                        <th>ประเภทการขาย</th>
                        <th>จำนวน</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </main>

    <div class="modal fade" id="passwordForm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Password</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('/')}}" method="post">
                    {{ csrf_field() }}
                <div class="modal-body">
                    <div id="showmsgpass">

                    </div>
                    <div class="row">
                        <div class="col-5 text-right">
                            <label for="newpass" class="font-weight-bold col-sm-12 col-form-label">new
                                password: </label>
                        </div>
                        <div class="col-7 form-group">
                            <input type="text" class="form-control" id="newpass">
                        </div>
                    </div>
                    <div id="showmsgconf">

                    </div>
                    <div class="row">
                        <div class="col-5 text-right">
                            <label for="confpass" class="font-weight-bold col-sm-12 col-form-label">confirm
                                password: </label>
                        </div>
                        <div class="col-7 form-group">
                            <input type="text" class="form-control" id="confpass">
                        </div>
                    </div>
                </div>
                <div class="modal-footer pt-1 pb-1">
                    <button type="button" class="btn btn-primary" id="okBtn">OK</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.modal -->

    <div class="modal fade" id="addSelfCost" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">เพิ่มค่าใช้จ่าย</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="content-modal">
                    <table class="table table-bordered table-striped" id="selftable">
                        <thead>
                        <tr>
                            <th>รายการ</th>
                            <th>จำนวนเงิน</th>
                        </tr>
                        </thead>
                        {{--<tbody>
                        @foreach($selfExpense as $self)
                            <tr>
                                <td>{{$self->list}}</td>
                                <td>{{$self->value}}</td>

                            </tr>

                        @endforeach
                        </tbody>--}}
                        <tfoot>
                        <th>รายการ</th>
                        <th>จำนวนเงิน</th>
                        </tfoot>
                    </table>
                    <div class="load">

                    </div>
                    <div class="showResult" id="loadShow">
                    </div>
                    <div class="row mt-2">
                        <div class="col-3 text-right">
                            <label for="listSelfCost" class="font-weight-bold col-sm-12 col-form-label">รายการ
                                : </label>
                        </div>
                        <div class="col-7 form-group">
                            <input type="text" class="form-control set" name="listSelfCost" id="listSelfCost">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-3 text-right">
                            <label for="selfCostPrice" class="font-weight-bold col-sm-12 col-form-label">จำนวนเงิน
                                : </label>
                        </div>
                        <div class="col-7 form-group">
                            <input type="number" class="form-control set" name="selfCostPrice" id="selfCostPrice">
                        </div>
                    </div>

                    <div class="row float-right pr-2 btn-again">
                        <button class="btn btn-info set" id="addSelfCostBtn">
                            เพิ่มรายการ
                        </button>
                    </div>
                </div>
                <div class="modal-footer pt-1 pb-1">
                    <button type="reset" id="cancelBtn" class="btn btn-secondary" data-dismiss="modal">close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- /.modal -->
@endsection

@section('footer')
    <script>
        $(document).ready(function () {
            $('#newpass,#confpass').keydown(function () {
                var newpass = $('#newpass').val();
                var confpass = $('#confpass').val();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    }
                });
                $.ajax({
                    url: 'checkpass',
                    method: 'POST',
                    data: {
                        newpass: newpass,
                        confpass: confpass,
                    },
                    dataType: 'HTML',
                    beforeSend: function () {

                    },
                    success: function () {
                        var check = '<?php if(isset($check)) echo $check;else "";?>';
                        var check2 = '<?php if(isset($check2)) echo $check2;else "";?>';

                        if(check != ""){
                            var htmlpass = "<ul><li>"+check+"</li></ul>";
                            $('#showmsgpass').html(htmlpass);
                        }
                        if(check2 != ""){
                            var htmlconf = "<ul><li>"+check2+"</li></ul>";
                            $('#showmsgconf').html(htmlconf);
                        }
                    }
                });
            });
        });
    </script>

    <script>
        function resetForm() {
            $('#listSelfCost').val('');
            $('#selfCostPrice').val('');
        }

        $(function () {
            $("#sellBalance").DataTable({
                "responsive": true,
                "autoWidth": false,
            });
        });
        $(function () {
            $("#monthSEll").DataTable({
                "responsive": true,
                "autoWidth": false,
            });
            var tables = $('#selftable').DataTable({
                pageLength: 4,
                searching: false,
                lengthChange: false,
                ordering: false,
                "autoWidth": false,
                ajax: '{{url('getData')}}',
                columns: [
                    {data: 'list', name: 'list'},
                    {data: 'value', name: 'value'},
                ],

            });
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#addSelfCostBtn').click(function () {
                var list = $('#listSelfCost').val();
                var price = $('#selfCostPrice').val();
                $.ajax({
                    url: 'checkExpense',
                    dataType: 'html',
                    data: {
                        listSelfCost: list,
                        selfCostPrice: price,
                    },
                    method: 'get',
                    beforeSend: function () {
                        var html = '<center><div class="loading"><img src="https://media1.giphy.com/media/y1ZBcOGOOtlpC/200.gif" alt=""></div></center>';
                        $('.load').html(html);
                        if (list == '' || price == '') {
                            var error = '<div class="alert alert-warning pb-0"><ul>';
                            if (list == '')
                                error += '<li>กรุณาใส่ชื่อรายการด้วย</li>';
                            if (price == '') {
                                error += '<li>กรุณาใส่จำนวนเงินด้วย</li>'
                            }
                            error += '</ul></div>';
                            $('.showResult').html(error);
                            $('.load').hide();
                            return false;
                        }
                    },
                    success: function () {
                        alert('success');
                        if ($('.showResult').has('alert alert-warning'))
                            $('.showResult').html('').removeClass('alert alert-warning');
                        swal("บันทึกข้อมูลสำเร็จ", "", "success");
                        tables.ajax.reload();
                        $('.load').hide();
                        resetForm();
                    }
                });

            });
        });

        $('#cancelBtn').click(function () {
            location.reload();
        });
    </script>
@endsection
