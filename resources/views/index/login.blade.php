<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @include('partials.head')
</head>
<body class="container bg-black ">
<main class="container" style="height: 100vh;">
    <div style="height: 10vh">

    </div>
    <div class="h-auto ml-auto mr-auto border-show-black col-5 bg-grey" style="border-color: #7d9726">
        <div class="row">
            <div class="col-12 card-header text-center">
                <label class="font-weight-bold cl-lightorange" style="font-size: 32px">Sign In</label>
            </div>
        </div>

        <div class="card-body">
            @if (session()->has('error'))
                <div class="alert alert-yellow">
                    <ul>
                        <li class="cl-danger">
                            {{session()->get('error')}}
                        </li>
                    </ul>
                </div>
            @endif
            <form action="{{route('checklogin')}}" method="post">
                @csrf
                <div class="form-group">
                    <label class="font-weight-bold" style="font-size: 24px;color: #C4CE8E">{{__('username :')}}</label><br>
                    @if ($errors->first('username') != "")
                            <ul class="alert alert-yellow ">
                                <li class="cl-danger">{{$errors->first('username')}}</li>
                            </ul>
                    @endif
                    <input type="text" name="username" id="username" class="form-control">
                </div>
                <div class="form-group">
                    <label class="font-weight-bold" style="font-size: 24px;color: #C4CE8E">{{__('password :')}}</label><br>
                    @if ($errors->first('password') != "")
                        <ul class="alert alert-yellow">
                            <li class="cl-danger">{{$errors->first('password')}}</li>
                        </ul>
                    @endif
                    <input type="password" name="password" id="password" class="form-control">
                </div>
                <div class="form-check checkbox-lg custom-checkbox pl-4">
                    <input class="form-check-input custom-control-input" type="checkbox" value="" id="savePassword" style="font-size: 2em">
                    <label class="form-check-label custom-control-label" for="savePassword">
                        จดจำรหัสผ่าน
                    </label>
                </div>

                <div class="form-group mt-4">
                    <input type="submit" value="login" class="button-theme col-12" >
                </div>

            </form>
        </div>

        {{--<form action="{{route('checklogin')}}" method="post">
            @csrf
            <div class="row pb-0">
                <div class="col-12 card-body text-left">
                    <label class="font-weight-bold" style="font-size: 24px;color: #C4CE8E">username :</label><br>
                    <input type="text" name="username" id="username" class="form-control">
                </div>
            </div>

            <div class="row pt-0 mt-0">
                <div class="col-12 card-body text-left">
                    <label class="font-weight-bold" style="font-size: 24px;color: #C4CE8E">password :</label><br>
                    <input type="password" name="password" id="password" class="form-control">
                </div>
            </div>

            <div class="row">
                <div class="col-12 card-body text-center">
                    <input type="submit" value="login" class="button-theme col-12" >
                </div>
            </div>
        </form>
--}}
    </div>

</main>
</body>
</html>

