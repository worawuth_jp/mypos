<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::any('/', 'SiteController@index')->name('/');

Route::resource('login','LoginController')->name('index','login');

Route::resource('checklogin','LoginController')->name('store','checklogin');

Route::get('inventory','SiteController@inventory')->name('inventory');

Route::get('category','SiteController@category')->name('category');

Route::get('stock','SiteController@stock')->name('stock');

Route::get('barcode','SiteController@barcode')->name('barcode');

Route::get('supply','SiteController@supply')->name('supply');

Route::get('finance','SiteController@finance')->name('finance');

Route::get('shopManagement','SiteController@shopManagement')->name('shopManagement');

Route::get('user','SiteController@user')->name('user');

Route::get('logout', 'SiteController@logout')->name('logout');

Route::any('checkExpense','SiteController@create')->name('checkExpense');

Route::any('getData','SelfCostController@index')->name('getData');

Route::post('checkpass','LoginController@create')->name('checkpass');
