<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $table = 'carts';


    public function users(){
        return $this->belongsTo(User::class,'users_id');
    }

    public function carts_products(){
        return $this->hasMany(CartProduct::class);
    }

}
