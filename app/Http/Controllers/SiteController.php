<?php

namespace App\Http\Controllers;

use App\SelfExpense;
use App\Http\Requests\validateRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SiteController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
    }

    public function index()
    {
        $date = date("Y-m-d");
        $selfExpense=null;
        $sum = 0;
        try {
            $selfExpense['data'] = DB::table('self_expenses')->whereDate('created_at','=',$date)
                ->where('users_id','=',Auth::user()->id)->where('isDelete','=',0)
                ->orderByDesc('id');
            $sum = DB::table('self_expenses')
                ->where('users_id','=',Auth::user()->id)
                ->where('isDelete','=',0)
                ->whereDate('created_at','=',$date)
                ->sum('value');
        } catch (\Exception $e) {
        }

        return view('index.index', [
            'selfExpense' => $selfExpense['data'],
            'sumcost'=>$sum,
        ]);
    }

    public function create(validateRequest $request)
    {
        $postlist = $request->post('listSelfCost');
        $postvalue = $request->post('selfCostPrice');
        if (isset($postlist) and isset($postvalue)) {
            $selfCost = new SelfExpense();
            $selfCost->users_id = Auth::user()->id;
            $selfCost->list = $request->post('listSelfCost');
            $selfCost->value = $request->post('selfCostPrice');
            //$selfCost->isDelete = 0;
            $selfCost->save();
        }

        $date = date("Y-m-d");
        $selfExpense=null;
        $sum = 0;
        try {
            $selfExpense['data'] = DB::table('self_expenses')->whereDate('created_at','=',$date)
                ->where('users_id','=',Auth::user()->id)->where('isDelete','=',0)
                ->orderByDesc('id');
            $sum = DB::table('self_expenses')
                ->where('users_id','=',Auth::user()->id)
                ->where('isDelete','=',0)
                ->whereDate('created_at','=',$date)
                ->sum('value');
        } catch (\Exception $e) {
        }

        return view('index.index', [
            'selfExpense' => $selfExpense['data'],
            'sumcost'=>$sum,
        ]);
    }

    public function inventory()
    {
        return view('manage.inventory');
    }

    public function category()
    {
        return view('manage.category');
    }

    public function stock()
    {
        return view('manage.stock');
    }

    public function barcode()
    {
        return view('manage.barcode');
    }

    public function supply()
    {
        return view('manage.supply');
    }

    public function finance()
    {
        return view('manage.finance');
    }

    public function shopManagement()
    {
        return view('manage.shopMangement');
    }

    public function user()
    {
        return view('user.user');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('login');
    }
}
