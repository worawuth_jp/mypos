<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        return view('index.login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $pass = $request->post('newpass');
        if(isset($pass)){
            if($pass != '')
            if(Hash::check($pass,Auth::user()->getAuthPassword())){
                return view('index.index',[
                    'check' => 'รหัสซ้ำกับรหัสเดิม'
                ])->with('check','รหัสซ้ำกับรหัสเดิม');
            }
        }
        $conf = $request->post('confpass');
        if(isset($conf)){
            if($conf != ''){
                if(strlen($conf) == strlen($pass) and $conf != $pass){
                    return view('index.index',[
                        'check2' => 'รหัสซ้ำกับรหัสเดิม'
                    ])->with('check2','รหัสซ้ำกับรหัสเดิม');
                }
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(LoginRequest $request)
    {
        $user=array(
            'username' => $request->post('username'),
            'password' => $request->post('password')
        );
        if(Auth::attempt($user)){
            return redirect('/');
        }
        else{
            return redirect('login')->with('failed','ไม่สามารถเข้าสู่ระบบได้');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
