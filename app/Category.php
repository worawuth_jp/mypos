<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';

    public function users(){
        return $this->belongsTo(User::class,'users_id');
    }
    public function products(){
        return $this->hasMany(Product::class);
    }
}
