<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CartProduct extends Model
{
    protected $table = 'carts_products';

    public function carts(){
        return $this->belongsTo(Cart::class,'carts_id');
    }
    public function products(){
        return $this->belongsTo(Product::class,'products_id');
    }
}
