<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authentication;

class User extends Authentication
{
    protected $table = 'users';

    public function barcodes(){
        return $this->hasMany(Barcode::class);
    }
    public function carts(){
        return $this->hasMany(Cart::class);
    }
    public function categories(){
        return $this->hasMany(Category::class);
    }
    public function discounts(){
        return $this->hasMany(Discount::class);
    }
    public function inventories(){
        return $this->hasMany(Inventory::class);
    }
    public function users(){
        return $this->hasMany(Product::class);
    }
    public function self_expenses(){
        return $this->hasMany(SelfExpense::class);
    }
    public function suppliers(){
        return $this->hasMany(Supplier::class);
    }

}
