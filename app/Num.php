<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Num extends Model
{
    protected $table = 'nums';

    public function products(){
        return $this->belongsTo(Product::class,'products_id');
    }
}
