<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cost extends Model
{
    protected $table = 'costs';

    public function products(){
        return $this->belongsTo(Product::class,'products_id');
    }
    public function suppliers(){
        return $this->hasOne(Supplier::class);
    }

}
