<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventorysProducts extends Model
{
    protected $table = 'inventories_products';

    public function inventories(){
        return $this->belongsTo(Inventory::class,'inventories_id');
    }
    public function products(){
        return $this->belongsTo(Product::class,'products_id');
    }
}
