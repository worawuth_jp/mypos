<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SelfExpense extends Model
{
    protected $table = 'self_expenses';
    protected $fillable = ['users_id','list','value','isDelete'];

    public function users(){
        return $this->belongsTo(User::class,'users_id');
    }
}
