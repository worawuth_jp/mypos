<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    protected $table = 'inventories';

    public function users(){
        return $this->belongsTo(User::class,'users_id');
    }
    public function inventories_products(){
        return $this->hasMany(InventorysProducts::class);
    }
}
