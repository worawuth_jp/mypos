<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    public function barcodes(){
        return $this->belongsTo(Barcode::class,'barcodes_id');
    }
    public function users(){
        return $this->belongsTo(User::class,'users_id');
    }
    public function categories(){
        return $this->belongsTo(Category::class,'categories_id');
    }
    public function carts(){
        return $this->hasMany(Cart::class);
    }
    public function costs(){
        return $this->hasOne(Cost::class);
    }
    public function carts_products(){
        return $this->hasMany(CartProduct::class);
    }
    public function inventories_products(){
        return $this->hasMany(InventorysProducts::class);
    }
    public function nums(){
        return $this->hasOne(Num::class);
    }
    public function supplier(){
        return $this->hasOne(Supplier::class);
    }
}
