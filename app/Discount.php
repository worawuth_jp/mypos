<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    protected $table = 'discounts';

    public function users(){
        return $this->belongsTo(User::class,'users_id');
    }

    public function barcodes(){
        return $this->belongsTo(Barcode::class,'barcodes_id');
    }
}
