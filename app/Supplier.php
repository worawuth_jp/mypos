<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected $table = 'suppliers';

    public function products(){
        return $this->belongsTo(Product::class,'products_id');
    }
    public function costs(){
        return $this->belongsTo(Cost::class,'costs_id');
    }
    public function users(){
        return $this->belongsTo(User::class,'users_id');
    }
}
