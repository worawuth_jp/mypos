<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barcode extends Model
{
    protected $table = 'barcodes';

    public function users(){
        return $this->belongsTo(User::class,'users_id');
    }
    public function discounts(){
        return $this->hasOne(Discount::class);
    }
    public function products(){
        return $this->hasOne(Product::class);
    }
}
